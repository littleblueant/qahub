ReleaseData.delete_all

[
  {id: 1,   section: "development",  label: "Development Checklist Item 1",  priority: 1,  active: 1},
  {id: 2,   section: "development",  label: "Development Checklist Item 2",  priority: 2,  active: 1},
  {id: 3,   section: "development",  label: "Development Checklist Item 3",  priority: 3,  active: 1},
  {id: 4,   section: "development",  label: "Development Checklist Item 4",  priority: 4,  active: 1},
  {id: 5,   section: "development",  label: "Development Checklist Item 5",  priority: 5,  active: 1},
  {id: 6,   section: "stage",        label: "Stage Checklist Item 1",        priority: 1,  active: 1},
  {id: 7,   section: "stage",        label: "Stage Checklist Item 2",        priority: 2,  active: 1},
  {id: 8,   section: "stage",        label: "Stage Checklist Item 3",        priority: 3,  active: 1},
  {id: 9,   section: "stage",        label: "Stage Checklist Item 4",        priority: 4,  active: 1},
  {id: 10,  section: "stage",        label: "Stage Checklist Item 5",        priority: 5,  active: 1},
  {id: 11,  section: "production",   label: "Production Checklist Item 1",   priority: 1,  active: 1},
  {id: 12,  section: "production",   label: "Production Checklist Item 2",   priority: 2,  active: 1},
  {id: 13,  section: "production",   label: "Production Checklist Item 3",   priority: 3,  active: 1},
  {id: 14,  section: "production",   label: "Production Checklist Item 4",   priority: 4,  active: 1},
  {id: 15,  section: "production",   label: "Production Checklist Item 5",   priority: 5,  active: 1},
  {id: 16,  section: "tools",        label: "ChecklistTool1",                priority: 1,  active: 1},
  {id: 17,  section: "tools",        label: "ChecklistTool2",                priority: 2,  active: 1},
  {id: 18,  section: "tools",        label: "ChecklistTool3",                priority: 3,  active: 1},
  {id: 19,  section: "tools",        label: "ChecklistTool4",                priority: 4,  active: 1},
  {id: 20,  section: "tools",        label: "ChecklistTool5",                priority: 5,  active: 1},
].each do |attr|
  ReleaseData.create(attr)
end
