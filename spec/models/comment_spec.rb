require 'spec_helper'

describe Comment do
  before { @comment = Comment.new(release_id: 1, checklist_item_id: 1, checklist_item_type: "Item", comment: "This is a test comment.", author: "Test User", modified_by: "testuser@example.com") }
  subject { @comment }
    
  it { should respond_to(:release_id) }
  it { should respond_to(:checklist_item_id) }
  it { should respond_to(:checklist_item_type) }
  it { should respond_to(:comment) }
  it { should respond_to(:author) }
  it { should respond_to(:modified_by) }
end