require 'spec_helper'

describe Release do
  before { @release = Release.new(title: "Fusion 13-1-1", contact: "Test User", backup_contact: "Test User 2", release_date: "2013-01-08", code_cutoff_date: "2013-01-01", modified_by: "testuser@exmple.com", archive: 0) }
  subject { @release }
    
  it { should respond_to(:title) }
  it { should respond_to(:contact) }
  it { should respond_to(:release_date) }
  it { should respond_to(:code_cutoff_date) }
  it { should respond_to(:backup_contact) }
  it { should respond_to(:modified_by) }
  it { should respond_to(:archive) }
end