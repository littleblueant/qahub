require 'spec_helper'

describe ChecklistItem do
  before { @checklist_item = ChecklistItem.new(release_id: 1, release_data_id: 1, environment: "development", value: 1, applicable: 1, modified_by: "testuser@example.com") }
  subject { @checklist_item }
    
  it { should respond_to(:release_id) }
  it { should respond_to(:release_data_id) }
  it { should respond_to(:environment) }
  it { should respond_to(:value) }
  it { should respond_to(:applicable) }
  it { should respond_to(:modified_by) }
end