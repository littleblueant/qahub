require 'spec_helper'

describe ChecklistTool do
  before { @checklist_tool = ChecklistItem.new(release_id: 1, release_data_id: 1, value: 1, modified_by: "example.com") }
  subject { @checklist_tool }
    
  it { should respond_to(:release_id) }
  it { should respond_to(:release_data_id) }
  it { should respond_to(:value) }
  it { should respond_to(:modified_by) }
end