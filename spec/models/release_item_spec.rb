require 'spec_helper'

describe ReleaseItem do
  before { @release_item = ReleaseItem.new(release_id: 1, item_type: "General", source: "VersionOne", source_reference: "D-11111", title: "Release Item 1", is_config_change: 0, is_data_change: 0, affected_product: "Fusion Website", level_of_comfort: 3, level_of_risk: 1, notes: "test user: this item has been checked in dev", modified_by: "testuser@example.com") }
  subject { @release_item }
    
  it { should respond_to(:release_id) }
  it { should respond_to(:item_type) }
  it { should respond_to(:source) }
  it { should respond_to(:source_reference) }
  it { should respond_to(:title) }
  it { should respond_to(:is_config_change) }
  it { should respond_to(:is_data_change) }
  it { should respond_to(:affected_product) }
  it { should respond_to(:level_of_comfort) }
  it { should respond_to(:level_of_risk) }
  it { should respond_to(:notes) }
  it { should respond_to(:modified_by) }
end