FactoryGirl.define do
  factory :release do |r|
    r.sequence(:title) { |n| "Test Release #{n}" }
    contact "Test User 1"
    backup_contact "Test User 2"
    release_date "2014-01-07"
    code_cutoff_date "2014-01-01"
    modified_by "testuser@example.com"
    
    factory :release_with_release_items do
      ignore do
        affected_browsers_data_count 20
        affected_designs_data_count 150
        release_items_count 40
      end
      
      after(:create) do |release, evaluator|
        FactoryGirl.create_list(:affected_browsers_data, evaluator.affected_browsers_data_count)
        FactoryGirl.create_list(:affected_designs_data, evaluator.affected_designs_data_count)
        FactoryGirl.create_list(:release_item, evaluator.release_items_count, release: release)
      end
    end
    
    factory :release_with_tools_and_checklist_items do
      ignore do
        checklist_tools_count 5
        checklist_items_count 15
      end
      
      after(:create) do |release, evaluator|
        FactoryGirl.create_list(:checklist_tool, evaluator.checklist_tools_count, release: release)
        FactoryGirl.create_list(:checklist_item, evaluator.checklist_items_count, release: release)
      end
    end
  end
  
  factory :affected_browsers_data do
    browser { ["IE 8", "IE 9", "IE 10", "FireFox", "Chrome", "Safari", "Mobile", "Tablet", "All", "None"].sample }
    label { ["IE 8", "IE 9", "IE 10", "FireFox", "Chrome", "Safari", "Mobile", "Tablet", "All", "None"].sample }
    priority { Random.new.rand(1..20) }
    active 1
  end
  
  factory :affected_designs_data do
    category { ["Acura", "Asbury", "BMW", "Dealer", "Facebok", "Ford", "General", "GSM", "Infiniti", "Lincoln", "Mobile", "Nissan", "Scion", "Tools"].sample }
    design { ["AcuraTestOne", "AcuraTestTwo", "AsburyTestOne", "AsburyTestTwo", "BMWTestOne", "BMWTestTwo", "DealerTestOne", "DealerTestTwo", "FacebokTestOne", "FacebokTestTwo", "FordTestOne", "FordTestTwo", "GeneralTestOne", "GeneralTestTwo", "GSMTestOne", "GSMTestTwo", "InfinitiTestOne", "InfinitiTestTwo", "LincolnTestOne", "InfinitiTestTwo", "MobileTestOne", "MobileTestTwo", "NissanTestOne", "NissanTestTwo", "ScionTestOne", "ScionTestTwo", "ToolsTestOne", "ToolsTestTwo"].sample }
    label { ["AcuraTestOne", "AcuraTestTwo", "AsburyTestOne", "AsburyTestTwo", "BMWTestOne", "BMWTestTwo", "DealerTestOne", "DealerTestTwo", "FacebokTestOne", "FacebokTestTwo", "FordTestOne", "FordTestTwo", "GeneralTestOne", "GeneralTestTwo", "GSMTestOne", "GSMTestTwo", "InfinitiTestOne", "InfinitiTestTwo", "LincolnTestOne", "InfinitiTestTwo", "MobileTestOne", "MobileTestTwo", "NissanTestOne", "NissanTestTwo", "ScionTestOne", "ScionTestTwo", "ToolsTestOne", "ToolsTestTwo"].sample }
    active 1
  end
  
  factory :release_item do |ri|
    release
    
    item_type "General"
    source { ["FogBugz", "VersionOne", "ZenDesk"].sample }
    source_reference "D-11111"
    ri.sequence(:title) { |n| "Release Item #{n}" }
    is_config_change { Random.new.rand(0..1) }
    is_data_change { Random.new.rand(0..1) }
    affected_browsers "FireFox, Chrome"
    affected_designs "DealerElite, DealerExtreme"
    affected_product "Fusion Website"
    level_of_comfort { Random.new.rand(1..3) }
    level_of_risk { Random.new.rand(1..3) }
    notes "test user: this item has been checked in dev"
    modified_by "testuser@example.com"
  end
  
  factory :checklist_tool do |t|
    release
    
    t.sequence(:release_data_id, 16) { |n| n }
    value { Random.new.rand(0..1) }
    modified_by "testuser@example.com"
  end
  
  factory :checklist_item do |ci|
    release

    ci.sequence(:release_data_id) { |n| n }
    environment { ["development", "stage", "production"].sample }
    value { Random.new.rand(0..1) }
    applicable { Random.new.rand(0..1) }
    modified_by "testuser@example.com"
  end
end