require 'spec_helper'
include Warden::Test::Helpers

describe "Checklist" do
  let(:base_title) { "QA Hub" }
  subject { page }
  
  describe "Show Page" do
    before(:all) { @release = FactoryGirl.create(:release_with_tools_and_checklist_items) }
    after(:all) { @release.destroy }
    
    describe "with non-signed in user" do
      before do
        logout(:user)
        visit checklist_path(@release.id)
      end
      
      it { should have_title("#{base_title} | Checklist for: #{@release.title}") }
      it { should have_selector("input[disabled='disabled']") }
      
      it { should_not have_selector("img[alt='Not Applicable']") }
      it { should_not have_selector("img[alt='Add a Comment']") }
    end
    
    describe "with signed in user" do
      before do
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit checklist_path(@release.id)
      end
      
      it { should have_title("#{base_title} | Checklist for: #{@release.title}") }
      it { should have_selector("img[alt='Not Applicable']") }
      it { should have_selector("img[alt='Add a Comment']") }
      
      describe "and add a new comment" do
        before do
          first(".actions").click_link("Add a Comment")
          fill_in "comment_comment", with: "Test Comment"
          click_button "Publish"
          visit checklist_path(@release.id)
        end
        
        it { should have_selector("div.comment.well", text: "Test Comment") }
      end
      
      describe "update a comment" do
        before do
          first(".actions").click_link("Add a Comment")
          fill_in "comment_comment", with: "Test Comment"
          click_button "Publish"
          visit checklist_path(@release.id)
          find(".comment.well > .heading > a:first").click
          fill_in "comment_comment", with: "Test Comment Again"
          click_button "Publish"
          visit checklist_path(@release.id)
        end
        
        it { should have_selector("div.comment.well", text: "Test Comment Again") }
      end
      
      describe "delete a comment" do
        before do
          first(".actions").click_link("Add a Comment")
          fill_in "comment_comment", with: "Test Comment"
          click_button "Publish"
          visit checklist_path(@release.id)
          find(".comment.well > .heading > a > img[@alt='Icondelete']").click
          visit checklist_path(@release.id)
        end
        
        it "should delete the comment" do
          expect { find(".comment.well > .heading > a:last").click }.to change(Comment, :count).by(-1)
        end
      end
    end
  end
end