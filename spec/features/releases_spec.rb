require 'spec_helper'
include Warden::Test::Helpers

describe "Releases" do
  let(:base_title) { "QA Hub" }
  subject { page }
  
  describe "Index Page" do
    describe "with non-signed in user" do
      before(:each) { FactoryGirl.create_list(:release, 25) }
      before do
        logout(:user)
        visit releases_path
      end
      
      it { should have_title("#{base_title} | Releases") }
      it { should have_link('2', href: '/releases?page=2') }
      
      it { should_not have_link('Update') }
      it { should_not have_link('Delete') }
      
      describe "viewing archived releases" do
        before do
          visit releases_path
          click_link "View All"
        end
        
        it { should have_link("Hide Archived") }
      end
    end
    
    describe "with signed in user" do
      before(:each) { FactoryGirl.create_list(:release, 25) }
      before do
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit releases_path
      end
      
      it { should have_title("#{base_title} | Releases") }
      it { should have_link('2', href: '/releases?page=2') }
      it { should have_link('Update') }
      it { should have_link('Delete') }
    end
  end
  
  describe "New Page" do
    describe "on create new release page" do
      before do
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit new_release_path
      end
      
      it { should have_title("#{base_title} | Create New Release") }
      
      describe "should create new release" do
        describe "with incorrect/missing data" do
          before do
            @user = FactoryGirl.create(:user, :admin)
            login_as @user, :scope => :user
            visit new_release_path
            click_button "Save"
          end
          
          it { should have_title("#{base_title} | Create New Release") }
          it { should have_selector("div.alert.alert-error", text: "Oops! Your form has 3 errors.") }
          it { should have_selector("li", text: "* Title can't be blank") }
        end
        
        describe "with correct data" do
          before do
            @user = FactoryGirl.create(:user, :admin)
            login_as @user, :scope => :user
            visit new_release_path
            fill_in "Title", with: "Test Release"
            select "Unassigned", from: "Primary Contact"
            select "Unassigned", from: "Backup Contact"
            fill_in "Release Date", with: "2013-01-08"
            fill_in "Code Cutoff Date", with: "2013-01-01"
            click_button "Save"
          end
          
          it { should have_title("#{base_title} | Releases") }
          it { should have_selector("div.alert.alert-success", text: "\"Test Release\" has been created.") }
        end
      end
      
      describe "should create and archive release" do
        before do
          @user = FactoryGirl.create(:user, :admin)
          login_as @user, :scope => :user
          visit new_release_path
          fill_in "Title", with: "Test Release"
          select "Unassigned", from: "Primary Contact"
          select "Unassigned", from: "Backup Contact"
          fill_in "Release Date", with: "2013-01-08"
          fill_in "Code Cutoff Date", with: "2013-01-01"
          check "release_archive"
          click_button "Save"
        end
        
        it { should have_title("#{base_title} | Releases") }
        it { should have_selector("div.alert.alert-success", text: "\"Test Release\" has been created.") }
        it { should have_selector("div.alert.alert-info", text: "\"Test Release\" has been archived.") }
      end
    end
  end
  
  describe "Edit Page" do
    describe "on releases page" do
      before do
        @release = FactoryGirl.create(:release)
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit releases_path
      end
      
      it "go to update release page" do
        click_link "Update"
        should have_title("#{base_title} | Update Release: Test Release")
      end
      
      describe "update release" do
        before do
          @user = FactoryGirl.create(:user, :admin)
          login_as @user, :scope => :user
          visit releases_path
        end
        
        describe "with incorrect/missing data" do
          before do
            click_link "Update"
            fill_in "Title", with: ""
            click_button "Save"
          end
          
          it { should have_title("#{base_title} | Update Release: ") }
          it { should have_selector("div.alert.alert-error", text: "Oops! Your form has 1 error.") }
          it { should have_selector("li", text: "* Title can't be blank") }
        end
        
        describe "with correct data" do
          before do
            click_link "Update"
          end
          
          it "should update release" do
            click_button "Save"
            should have_title("#{base_title} | Releases")
            should have_selector("div.alert.alert-success", text: "\"#{@release.title}\" has been updated.")
          end
          
          it "should archive release" do
            check "release_archive"
            click_button "Save"
            should have_title("#{base_title} | Releases")
            should have_selector("div.alert.alert-success", text: "\"#{@release.title}\" has been updated.")
            should have_selector("div.alert.alert-info", text: "\"#{@release.title}\" has been archived.")
          end
        end
      end
    end
  end
  
  describe "Delete Page" do
    describe "on releases page" do
      before do
        @release = FactoryGirl.create(:release)
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit releases_path
      end
      
      it { should have_title("#{base_title} | Releases") }
      
      describe "delete release" do
        before do
          @user = FactoryGirl.create(:user, :admin)
          login_as @user, :scope => :user
          visit releases_path
        end
        
        it { should have_title("#{base_title} | Releases") }
        it "should delete release" do
          expect { click_link "Delete" }.to change(Release, :count).by(-1)
          should have_title("#{base_title} | Releases")
          should have_selector("div.alert.alert-success", text: "\"#{@release.title}\" has been deleted.")
        end
      end
    end
  end
end