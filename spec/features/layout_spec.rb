require 'spec_helper'
include Warden::Test::Helpers

describe "Layout" do
  before { logout(:user) }
  subject { page }
  
  describe "Navigation Menu with signed out user" do
    before { visit root_path }
    
    it { should have_selector('a', text: 'Admin') }
    it { should have_link('Sign In', href: new_user_session_path) }
    
    it { should_not have_link('QA Confluence Page', href: "http://confluence.clickmotive.com/display/qa/Home") }
    it { should_not have_link('Profile', href: edit_user_registration_path) }
    it { should_not have_link('Sign Out', href: destroy_user_session_path) }
  end
  
  describe "Navigation Menu with signed in user" do
    before do
      @user = FactoryGirl.create(:user, :admin)
      login_as @user, :scope => :user
      visit root_path
    end
    
    it { should have_selector('a', text: "#{@user.name}") }
    it { should have_link('QA Confluence Page', href: "http://confluence.clickmotive.com/display/qa/Home") }
    it { should have_link('Profile', href: edit_user_registration_path) }
    it { should have_link('Sign Out', href: destroy_user_session_path) }
    
    it { should_not have_selector('a', text: 'Admin') }
    it { should_not have_link('Sign In', href: new_user_session_path) }
  end
end
