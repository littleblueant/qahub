require 'spec_helper'
include Warden::Test::Helpers

describe "Release Items" do
  let(:base_title) { "QA Hub" }
  subject { page }
  
  describe "Index Page" do
    describe "with non-signed in user" do
      before(:each) { @release = FactoryGirl.create(:release_with_release_items) }
      before do
        logout(:user)
        visit release_items_path(id: @release.id)
      end
      
      it { should have_title("#{base_title} | Release Items for: #{@release.title}") }
      
      it { should_not have_link('Update') }
      it { should_not have_link('Delete') }
    end
    
    describe "with signed in user" do
      before(:each) { @release = FactoryGirl.create(:release_with_release_items) }
      before do
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit release_items_path(id: @release.id)
      end
      
      it { should have_title("#{base_title} | Release Items for: #{@release.title}") }
      it { should have_link('Update') }
      it { should have_link('Delete') }
    end
      
    describe "send patch list email" do
      before do
        @release = FactoryGirl.create(:release_with_release_items)
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit release_items_path(id: @release.id)
        click_link "Email Patch List"
      end
      
        it { should have_selector("div.alert.alert-info", text: "The patch list for #{@release.title} has been sent") }
    end
  end
  
  describe "New Page" do
    describe "on create new release item page" do
      before do
        @release = FactoryGirl.create(:release_with_release_items)
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit new_release_item_path
      end
      
      it { should have_title("#{base_title} | Create New Release Item") }
      
      describe "should create a new release item" do
        describe "with incorrect data" do
          before do
            @user = FactoryGirl.create(:user, :admin)
            login_as @user, :scope => :user
            visit new_release_item_path
            click_button "Save"
          end
          
          it { should have_title("#{base_title} | Create New Release Item") }
          it { should have_selector("div.alert.alert-error", text: "Oops! Your form has 4 errors.") }
          it { should have_selector("li", text: "* Title can't be blank") }
        end
        
        describe "with correct data" do
          before do
            @user = FactoryGirl.create(:user, :admin)
            login_as @user, :scope => :user
            visit new_release_item_path
            select "General", from: "Type"
            select "VersionOne", from: "Source"
            fill_in "Source Reference", with: "D-11111"
            fill_in "Title", with: "Test Release Item"
            choose "release_item_is_data_change_false"
            choose "release_item_is_config_change_false"
            first("div.affected-browsers-list > label > input").set(true)
            first("div.affected-designs-list > label > input").set(true)
            select "Fusion Website", from: "Affected Product"
            select "low", from: "release_item[level_of_risk]"
            select "high", from: "release_item[level_of_comfort]"
            fill_in "Notes", with: "This is a test note."
            find("#release_item_modified_by").set(@user.email)
            find("#release_item_release_id").set(@release.id)
            click_button "Save"
          end
          
          it { should have_title("#{base_title} | Release Items for: #{@release.title}") }
          it { should have_selector("div.alert.alert-success", text: "\"Test Release Item\" has been created as a new release item.") }
        end
      end
    end
  end
  
  describe "Edit Page" do
    describe "on release items page" do
      before do
        @release = FactoryGirl.create(:release_with_release_items)
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit release_items_path(id: @release.id)
      end
      
      it "go to update release item page" do
        first(".item-title-actions").click_link("Update")
        should have_title("#{base_title} | Update Release Item")
      end
      
      describe "update release item" do
        before do
          @user = FactoryGirl.create(:user, :admin)
          login_as @user, :scope => :user
          visit release_items_path(id: @release.id)
          first(".item-title-actions").click_link("Update")
          first("div.affected-browsers-list > label > input").set(true)
          first("div.affected-designs-list > label > input").set(true)
        end
        
        describe "should update release item" do
          describe "with incorrect data" do
            before do
              fill_in "Title", with: ""
              click_button "Save"
            end
            
            it { should have_selector("div.alert.alert-error", text: "Oops! Your form has 1 error.") }
            it { should have_selector("li", text: "* Title can't be blank") }
          end
          
          describe "with correct data" do
            before do
              click_button "Save"
            end
            
            it { should have_selector("div.alert.alert-success") }
          end
        end
      end
    end
  end
  
  describe "Delete Page" do
    describe "on release items page" do
      before do
        @release = FactoryGirl.create(:release_with_release_items)
        @user = FactoryGirl.create(:user, :admin)
        login_as @user, :scope => :user
        visit release_items_path(id: @release.id)
      end
      
      it { should have_title("#{base_title} | Release Items for: #{@release.title}") }
      
      describe "delete release item" do
        before do
          @user = FactoryGirl.create(:user, :admin)
          login_as @user, :scope => :user
          visit release_items_path(id: @release.id)
        end
        
        it { should have_title("#{base_title} | Release Items for: #{@release.title}") }
        it "should delete release item" do
          expect { first(".item-title-actions").click_link("Delete") }.to change(ReleaseItem, :count).by(-1)
          should have_title("#{base_title} | Release Items for: #{@release.title}")
          should have_selector("div.alert.alert-success")
        end
      end
    end
  end
  
  describe "CSV Export" do
    # TODO: test csv export functionality
  end
end