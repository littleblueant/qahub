class ReportController < ApplicationController
  def report
    @report = Report.new
    @release = Release.find(params[:id])
  end
  
  def report_email
    @report = Report.new(params[:report])
    @release = Release.find(@report.release_id)
    
    if @report.valid?
      ReportMailer.send_report(@report, @release).deliver
      flash[:info] = "The post release report has been sent out."
      redirect_to releases_path
    else
      render 'report'
    end
  end
end
