function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


/*
	Disabling the collapsing functionality for now
	so that I can push this out to production now.
	
	TODO: Finish this functionality later.
*/
// function collapseDetails(id) {
	// $('#item_' + id).find('.collapsible').slideToggle('fast');
	// if($('#item_' + id).find('.collapsible-link').text() == 'Details -'){
		// $('#item_' + id).find('.collapsible-link').text('Details +');
	// }else{
		// $('#item_' + id).find('.collapsible-link').text('Details -');
	// }
// }
// 
// function collapseAllDetailsUp() {
	// $('article').find('.collapsible').slideUp('fast');
	// $('article').each(function(){
		// $(this).find('.collapsible-link').text('Details +');
	// });
	// $('.collapse-all-up').addClass('hide');
	// $('.collapse-all-down').removeClass('hide');
// }
// 
// function collapseAllDetailsDown() {
	// $('article').find('.collapsible').slideDown('fast');
	// $('article').each(function(){
		// $(this).find('.collapsible-link').text('Details -');
	// });
	// $('.collapse-all-down').addClass('hide');
	// $('.collapse-all-up').removeClass('hide');
// }


$(document).ready(function(){
	$('.data-list.release-items-list').sortable({items: '> .data'}).bind('sortupdate', function() {
	    var release = getUrlVars()["id"];
	    $.ajax({
			type: 'POST',
			data: $(this).sortable("serialize"),
			url: '/release_items/'+ release +'/prioritize',
			error: function() { alert('There was an error contacting the database. Upon closing this dialog your page will be refreshed automatically. If after the refresh you are not able to access the QA Hub you may have a problem with you internet connection.\r\nNOTE: If you are using VPN to access the QA Hub please try disconnecting / connecting your VPN connection.'); location.reload(); }
		});
	});
});