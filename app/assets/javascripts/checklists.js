var toggleComplete = function toggleComplete(section){
	if($('.checklist-'+section).find('input[type=checkbox]:not(:disabled):not(:checked)').length == 0){
		$('.checklist-'+section).find('.status').html('<span class="complete">Complete</span>');
	} else {
		$('.checklist-'+section).find('.status').html('<span class="incomplete">Incomplete</span>');
	}
}

var toggleItemCheckbox = function toggleItemCheckbox(id,section){
	$.ajax({
		type: 'GET',
		url: '/checklists/'+ id +'/toggle_item',
		success: function() { toggleComplete(section) },
		error: function() { alert('There was an error contacting the database. Upon closing this dialog your page will be refreshed automatically. If after the refresh you are not able to access the QA Hub you may have a problem with you internet connection.\r\nNOTE: If you are using VPN to access the QA Hub please try disconnecting / connecting your VPN connection.'); location.reload(); }
	});
}

var commentsToggle = function commentsToggle(id,count){
	$('.comments_block_' + id).slideToggle('fast');
	if($('.comments-toggle_' + id).text() == "Hide Comments (" + count + ")"){
    	$('.comments-toggle_' + id).text('Show Comments (' + count + ')');
    }else{
    	$('.comments-toggle_' + id).text('Hide Comments (' + count + ')');
    }
}

$(document).ready(function(){
	toggleComplete('development');
	toggleComplete('stage');
	toggleComplete('production');
});
