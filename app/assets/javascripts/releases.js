function setCookie(c_name,value,exdays) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1) {
		c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1) {
		c_value = null;
	} else {
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		if (c_end == -1) {
			c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}

function toggleReleaseView(view) {
	if (getCookie("release_view")) {
		if (getCookie("release_view") == "detail") {
			$('.list-view-link').removeClass('btn-primary');
			$('.detail-view-link').addClass('btn-primary');
			$('#list-view').fadeOut(200, function(){
				$('#detail-view').fadeIn(200);
			});
		} else if (getCookie("release_view") == "list") {
			$('.detail-view-link').removeClass('btn-primary');
			$('.list-view-link').addClass('btn-primary');
			$('#detail-view').fadeOut(200, function(){
				$('#list-view').fadeIn(200);
			});
		}
	} else {
		$('.list-view-link').removeClass('btn-primary');
		$('.detail-view-link').addClass('btn-primary');
		$('#list-view').fadeOut(200, function(){
			$('#detail-view').fadeIn(200);
		});
	}
}

$(document).ready(function(){
	if (!getCookie("release_view")) {
		setCookie("release_view","detail",365);
		toggleReleaseView();
	} else {
		toggleReleaseView();
	}
	
	$('[data-behaviour="datepicker"]').datepicker({
	    'format': 'yyyy-mm-dd',
	    'weekStart': 1,
	    'autoclose': true
	});
	
	$('.detail-view-link').each(function(){
		$(this).click(function(event){
			setCookie("release_view","detail",365);
			toggleReleaseView();
			event.preventDefault();
		});
	});
	
	$('.list-view-link').each(function(){
		$(this).click(function(event){
			setCookie("release_view","list",365);
			toggleReleaseView();
			event.preventDefault();
		});
	});
});