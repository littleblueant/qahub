class ReleaseData < ActiveRecord::Base
  attr_accessible :id, :section, :label, :priority, :active
  
  has_many :checklist_tools
  has_many :checklist_items
end