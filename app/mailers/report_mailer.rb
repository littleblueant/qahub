class ReportMailer < ActionMailer::Base
  def send_report(report, release)
    @report = report
    @release = release
    mail from: "ReleaseManagement@ClickMotive.com", to: "INT-Technology@dealertrack.com", subject: "Post Release Report for #{@release.title}"
  end
end