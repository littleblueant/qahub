class ReleaseMailer < ActionMailer::Base
  def send_patch_list(release, patch_list, sent_by)
    @release = release
    @patch_list = patch_list
    @sent_by = sent_by
    mail from: "QA@ClickMotive.com", to: "INT-Technology@dealertrack.com", subject: "Patch list for #{@release.title}"
  end
end