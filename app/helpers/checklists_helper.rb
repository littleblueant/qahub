module ChecklistsHelper
	def toggle_tool(tool)
	  tool.value? ? "<a href='/checklists/#{tool.id}/toggle_tool' data-remote='true' id='checklist_tool_#{tool.id}' class='tool-affected'>#{tool.release_data.label}</a>".html_safe : "<a href='/checklists/#{tool.id}/toggle_tool' data-remote='true' id='checklist_tool_#{tool.id}' class='tool-not-affected'>#{tool.release_data.label}</a>".html_safe
	end
	
	def toggle_item_checkbox(item)
	  @environment = "'#{item.environment}'"
	  if user_signed_in?
      @disabled = ""
	  else
	    @disabled = "disabled='disabled'"
	  end
	  
		if item.value == true
		  if item.applicable == true
		    "<input type='checkbox' id='checklist_item_#{item.id}' data-id='#{item.id}' onclick=\"toggleItemCheckbox(#{item.id},#{@environment})\" checked='checked' #{@disabled} /> #{item.release_data.label}".html_safe
		  elsif item.applicable == false
		    "<input type='checkbox' id='checklist_item_#{item.id}' data-id='#{item.id}' onclick=\"toggleItemCheckbox(#{item.id},#{@environment})\" checked='checked' disabled='disabled' /> #{item.release_data.label}".html_safe
		  end
    elsif item.value == false
		  if item.applicable == true
		    "<input type='checkbox' id='checklist_item_#{item.id}' data-id='#{item.id}' onclick=\"toggleItemCheckbox(#{item.id},#{@environment})\" #{@disabled} /> #{item.release_data.label}".html_safe
		  elsif item.applicable == false
		    "<input type='checkbox' id='checklist_item_#{item.id}' data-id='#{item.id}' onclick=\"toggleItemCheckbox(#{item.id},#{@environment})\" disabled='disabled' /> #{item.release_data.label}".html_safe
		  end
	  end
	end
	
	def toggle_applicable(item)
		item.applicable? ? "<img src='/assets/iconNotApplicable.png' alt='Not Applicable' /> Mark Not Applicable".html_safe : "<img src='/assets/iconSave.png' alt='Applicable' /> Mark Applicable".html_safe
	end
end